require('./config/config.js');
const express = require('express');
const search = require('./lib/logic/search.js');
const submit = require('./lib/submit_api/submit_api_client.js');


const app = express();
const defaultPort = 9999;

app.get('/', async function (req, res) {
    try {
        const search_result = await search.search();
        const mappedResult = submit.map(search_result.result);
        submit.submit(search_result.search_text, mappedResult);
        res.send(mappedResult);
    }
    catch (err) {
        res.status(500).send(`Something went wrong.  Failed to retrieve a response. ${err}`);
    }
});

app.listen(global.rekon_config.node_port || defaultPort);