const assert = require('assert');
const submit = require('../lib/submit_api/submit_api_client.js')

describe('Array', async function() {   

    it('should return mapped result with index', async function() {
        const input = [{ subtext: 'ef', indexes: [6] }];
        const result = submit.map(input);
        assert.deepStrictEqual(result, [{ subtext: 'ef', result: [6] }]);
    });

    it('should return mapped result with no output', async function() {
        const input = [{ subtext: 'ef', indexes: [] }];
        const result = submit.map(input);
        assert.deepStrictEqual(result, [{ subtext: 'ef', result: '<No Output>' }]);
    });    

    it('should return mapped result with both values', async function() {
        const input = [{ subtext: 'Ef', indexes: [6] }, { subtext: 'ef', indexes: [] }];
        const result = submit.map(input);
        assert.deepStrictEqual(result, [{ subtext: 'Ef', result: [6] }, { subtext: 'ef', result: '<No Output>' }]);
    });

});
