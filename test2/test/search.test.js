const assert = require('assert');
const search = require('../lib/logic/search.js')

describe('Array', async function() {

    it('should return empty list when given empty search string', async function() {
        const result = search.search_list('', ['peter']);
        assert.strictEqual(result.length, 0);
    });

    it('should return empty list when given empty item search list', async function() {
        const result = search.search_list('abc', []);
        assert.strictEqual(result.length, 0);
    });

    it('should return position 1 when the word contains one letter with a single letter search item', async function() {
        const result = search.search_list('a', ['a']);
        assert.strictEqual(result.length, 1);
        assert.deepStrictEqual(result, [{ subtext: 'a', indexes: [1] }]);
    });

    it('should return position 1 and 4 when the word contains 2 of the search letters with a single letter search item', async function() {
        const result = search.search_list('abca', ['a']);
        assert.strictEqual(result.length, 1);
        assert.deepStrictEqual(result, [{ subtext: 'a', indexes: [1, 4] }]);
    });

    it('should return a result for 2 search items when the word contains both of the search letters with multiple letter search item', async function() {
        const result = search.search_list('abca', ['a', 'c']);
        assert.strictEqual(result.length, 2);
        assert.deepStrictEqual(result, [{ subtext: 'a', indexes: [1, 4] }, { subtext: 'c', indexes: [3] }]);
    });

    it('should return a result for 2 search items one with a list of 0 matches', async function() {
        const result = search.search_list('abca', ['a', 'n']);
        assert.strictEqual(result.length, 2);
        assert.deepStrictEqual(result, [{ subtext: 'a', indexes: [1, 4] }, { subtext: 'n', indexes: [] }]);
    });    

    it('should return a result of 1 full word', async function() {
        const result = search.search_list('abc def ann', ['ef']);
        assert.strictEqual(result.length, 1);
        assert.deepStrictEqual(result, [{ subtext: 'ef', indexes: [6] }]);
    });

});
