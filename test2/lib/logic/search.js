module.exports = {
    search: async function() {
        const sub_text_api = require('../sub_text_api/sub_text_api_client');
        const text_search_api = require('../text_search_api/text_search_api_client');

        const sub_text_promise = sub_text_api.get_sub_texts();
        const text_search_promise = text_search_api.get_text();
        const promise_results = await Promise.all([sub_text_promise, text_search_promise]);

        if (!promise_results[0]) {
            throw 'No information returned from sub text service';
        }

        if (!promise_results[1]) {
            throw 'No information returned from text search service';
        }

        const search_result = this.search_list(promise_results[1].text, promise_results[0].subTexts);
        const result = {search_text: promise_results[1].text, result: search_result};
        return Promise.resolve(result);
    },    

    search_list: function(search_text, search_items) {
        const result = [];

        for (var search_position_cnt = 0; search_position_cnt < search_text.length; search_position_cnt++) {
            if (search_text[search_position_cnt] === ' ') {
                continue;
            }

            for (var search_item_cnt = 0; search_item_cnt < search_items.length; search_item_cnt++) {
                var search_item_result = result.find(x => x.subtext === search_items[search_item_cnt]);
                if (!search_item_result) {
                    search_item_result = {subtext: search_items[search_item_cnt], indexes: []};
                    result.push(search_item_result);
                }
                let counter = 0;
                while (counter < search_items[search_item_cnt].length) {
                    if (search_text[search_position_cnt + counter].toLowerCase() === search_items[search_item_cnt][counter].toLowerCase()) {
                        if (counter === search_items[search_item_cnt].length - 1) {
                            search_item_result.indexes.push(search_position_cnt + 1);
                        }
                        counter++;
                    }
                    else {
                        break;
                    }
                }
            }
        }
        return result;
    }  
}