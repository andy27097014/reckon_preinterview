const fetch = require('node-fetch');

const end_point = global.rekon_config.text_to_search_api_endpoint;

module.exports =  {
    get_text: async function () {
        if (!end_point) {
            throw 'Text Search endpoint has not been defined.'
        }

        try {
            var isSuccess = false;
            while (!isSuccess) {
                var result = await fetch(end_point);
                if (result.ok) {
                    isSuccess = true;
                    var jsonResult = await result.json();
                    return Promise.resolve(jsonResult);
                }
                if (result.statusCode === 404) {
                    throw `Error returned from Text Search endpoint, status text ${result.statusText} not found.`;
                }
            }
        }
        catch (err) {
            console.log('Error in get_text in text_search_api_client.', err);
            throw err;
        }        
    }
}