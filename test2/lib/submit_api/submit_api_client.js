const fetch = require('node-fetch');


module.exports =  {
    submit: async function (text, search_results) {
        const end_point = global.rekon_config.submit_api_endpoint;
        if (!end_point) {
            throw 'Submit endpoint has not been defined.'
        }

        const body = {
            candidate: 'Andrew Luck',
            text: text,
            results: search_results
        };

        try {            
            var isSuccess = false;
            while (!isSuccess) {
                var result = await fetch(end_point, {
                    method: 'post',
                    body:    JSON.stringify(body),
                    headers: { 'Content-Type': 'application/json' },
                });
                if (result.ok) {
                    isSuccess = true;
                    var jsonResult = await result.json();
                    return Promise.resolve(jsonResult);
                }
                if (result.statusCode === 404) {
                    throw `Error returned from Submit endpoint, status text ${result.statusText} not found.`;
                }
            }
        }
        catch (err) {
            console.log('Error in submit in submit_api_client.', err);
            throw err;
        }        
    },

    map: function(input) {
        return input.map(x => { 
            var result = x.indexes;
            if (result.length === 0) {
                result = '<No Output>';
            }
            return {subtext: x.subtext, result: result};
        });
    }
}