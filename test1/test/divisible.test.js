const assert = require('assert');
const divisor = require('../lib/logic/divisible.js')

describe('Array', async function() {

    it('should return empty list when given no start value', async function() {
        const result = divisor.calculateDivisible(null, 1, []);
        assert.strictEqual(result.length, 0);
    });

    it('should return empty list when given no end value', async function() {
        const result = divisor.calculateDivisible(1, null, []);
        assert.strictEqual(result.length, 0);
    });

    it('should return empty list when given start value is greater than end value', async function() {
        const result = divisor.calculateDivisible(4, 1, []);
        assert.strictEqual(result.length, 0);
    });

    it('should return list with all arrays empty when passed in no divisors', async function() {
        const result = divisor.calculateDivisible(1, 2, []);
        assert.strictEqual(result.length, 2);
        assert.deepStrictEqual(result, [{id: 1, items: []}, {id: 2, items: []}]);
    });

    it('should return list with correct results when passed in a single divisor', async function() {
        const result = divisor.calculateDivisible(1, 4, [{ divisor: 2, output: 'Boss'}]);
        assert.strictEqual(result.length, 4);
        assert.deepStrictEqual(result, [{id: 1, items: []}, {id: 2, items: ['Boss']}, {id: 3, items: []}, {id: 4, items: ['Boss']}]);
    });

    it('should return list with correct results when passed in multipls divisors', async function() {
        const result = divisor.calculateDivisible(1, 4, [{ divisor: 2, output: 'Boss'}, { divisor: 4, output: 'Hog'}]);
        assert.strictEqual(result.length, 4);
        assert.deepStrictEqual(result, [{id: 1, items: []}, {id: 2, items: ['Boss']}, {id: 3, items: []}, {id: 4, items: ['Boss', 'Hog']}]);
    });

});
