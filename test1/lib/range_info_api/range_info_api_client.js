const fetch = require('node-fetch');

const end_point = global.rekon_config.range_api_endpoint;

module.exports =  {
    get_range: async function () {
        if (!end_point) {
            throw 'Range Info endpoint has not been defined.'
        }

        try {
            var isSuccess = false;
            while (!isSuccess) {
                var result = await fetch(end_point);
                if (result.ok) {
                    isSuccess = true;
                    var jsonResult = await result.json();
                    return Promise.resolve(jsonResult);
                }
                if (result.statusCode === 404) {
                    throw `Error returned from Range Info endpoint, status text ${result.statusText} not found.`;
                }
            }
        }
        catch (err) {
            console.log('Error in get_range in range_info_api_client.', err);
            throw err;
        }
        
    }
}