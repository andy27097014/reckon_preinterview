module.exports = {
    get: async function() {
        const range_api = require('../range_info_api/range_info_api_client');
        const divisor_api = require('../divisor_info_api/divisor_info_api_client');

        const range_result_promise = range_api.get_range();
        const divisor_result_promise = divisor_api.get_divisors();
        const promise_results = await Promise.all([range_result_promise, divisor_result_promise]);

        if (!promise_results[0]) {
            throw 'No information returned from range info service';
        }

        if (!promise_results[1]) {
            throw 'No information returned from divisor info service';
        }

        const result = this.calculateDivisible(promise_results[0].lower, promise_results[0].upper, promise_results[1].outputDetails);
        return Promise.resolve(result);
    },    

    calculateDivisible: function(start_value, end_value, divisors) {
        result = [];
        if (start_value !== null && start_value !== undefined && end_value !== null && end_value !== undefined && start_value <= end_value) {
            for (var range_count = start_value;  range_count <= end_value; range_count++) {
                result.push({ id: range_count, items: [] });
                if (!!divisors) {
                    for(var divisor_count = 0; divisor_count < divisors.length; divisor_count++) {
                        if (!!divisors[divisor_count].divisor && range_count % divisors[divisor_count].divisor === 0) {
                            result.find(x => x.id === range_count).items.push(divisors[divisor_count].output);
                        }
                    }
                }
            }
        }

        return result;
    }   
}