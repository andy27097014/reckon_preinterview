require('./config/config.js');
const express = require('express');
const divisible = require('../lib/logic/divisible.js');


const app = express();
const defaultPort = 9999;

app.get('/', async function (req, res) {
    try {
        const divisible_result = await divisible.get();
        let response = `
            <html>
                ${divisible_result.reduce((a, b) => {
                    return `${a}<br/>${b.id}:${b.items.join('')}`;
                }, '')}
            </html>
        `;
        res.send(response);
    }
    catch (err) {
        res.status(500).send(`Something went wrong.  Failed to retrieve a response. ${err}`);
    }
});

app.listen(global.rekon_config.node_port || defaultPort);